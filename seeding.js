const Item = require("./models/Item");

const itemsToSeed = [
    { Price: 200, Name: "PC", Picture: "https://picsum.photos/id/2/600/600" },
    { Price: 60, Name: "Shoes", Picture: "https://picsum.photos/id/21/600/600" },
    { Price: 20, Name: "Cactus", Picture: "https://picsum.photos/id/248/600/600" },
];

const seedDB = async () => {
    const numOfItems = await Item.countDocuments({});
    if (numOfItems < 3) {
        Item.insertMany(itemsToSeed);
        console.log("Seeding");
    }
}

module.exports = seedDB;
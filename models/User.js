const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
    UserName: {
        type: String,
        unique: true
    },
    Password: {
        type: String,
    }
});

module.exports = mongoose.model("User", userSchema);
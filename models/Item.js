const mongoose = require('mongoose');
const { Schema } = mongoose;

const itemSchema = new Schema({
    Price: {
        type: Number,
        min: 0,
    },
    Name: String,
    Picture: String
});

module.exports = mongoose.model("Item", itemSchema);
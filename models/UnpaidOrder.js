const mongoose = require('mongoose');
const { Schema, ObjectId } = mongoose;
const Item = require("./Item");
const User = require("./User");

const unpaidOrderSchema = new Schema({
    Items: [{
        ItemToBuy: {
        type: ObjectId,
        ref: Item
        }, 
        Amount: Number
    }],
    TotalCost: {
        type: Number,
        min: 0
    },
    User: {
        type: ObjectId,
        ref: User
    }
}, {timestamps: true});

module.exports = mongoose.model("UnpaidOrder", unpaidOrderSchema);
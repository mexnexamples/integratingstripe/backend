# Express/mongoose backend for Stripe integration

The corresponding frontend can be found [here](https://gitlab.com/mexnexamples/integratingstripe)

## Steps to use
* After downloading, create a .env file inside the backend folder
* Then set the PORT, MONGODBURL and SECRET_STRIPE_KEY variables
* Finally, open the console and type npm install, then npm start

## Examples of .env variables
* PORT=8000
* MONGODBURL="mongodb+srv://admin:admin@cluster0.fbyr4.mongodb.net/dbName?retryWrites=true&w=majority"
* SECRET_STRIPE_KEY="sk_test_4S68v29DeKcE4RxJcrJnUn5s"
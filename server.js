const dotenv = require("dotenv").config({ path: "./.env" });
const cors = require("cors");
const express = require("express");
const stripe = require("stripe")(process.env.SECRET_STRIPE_KEY);   // This is a generic key used in the Stripe examples, fill it with your own secret key
const mongoose = require("mongoose");

const calculateCost = require("./calculateCost");
const Item = require("./models/Item");
const UnpaidOrder = require("./models/UnpaidOrder");
const PaidOrder = require("./models/PaidOrder");
const User = require("./models/User");
const seedDB = require("./seeding");

const app = express();
app.use(express.json());
app.use(cors());

mongoose.connect(process.env.MONGODBURL, 
  {
        useNewUrlParser: true,
        useUnifiedTopology: true    
  }
)
.then(() => {
  seedDB();
  console.log("Database connected");
})
.catch(err => console.log(`DB connection error: ${err}`));

app.get("/api/allitems", async (req, res) => {
  const allItems = await Item.find({}).exec();
  res.json(allItems);
})

app.post("/loginorregister", async (req, res) => {                                  // Replace this with any valid authentication system, like Passport, Firebase etc
  const user = req.body;
  try {
    const userInDB = await User.findOne({UserName: user.UserName}).exec();
    if (!userInDB) {
      await new User(user).save();
      res.status(200).json({message: "User created"});
    } else {
      userInDB.Password === user.Password ? res.status(200).json({message: "Logged in successfully"}) : res.status(401).json({errorMessage: "Wrong password"});
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({errorMessage: error});
  }
});

app.post("/create-payment-intent", async (req, res) => {
  const items = req.body.items;
  const user = req.body.user;
  try {
    const unpaidOrder = await calculateCost(items, user);
    // Create a PaymentIntent with the order amount and currency
    const paymentIntent = await stripe.paymentIntents.create({
      amount: unpaidOrder.TotalCost,
      currency: "usd"                                               // Change this to the user's currency, by getting country/currency from req.body
    });
    res.json({
      clientSecret: paymentIntent.client_secret,
      unpaidOrder
    }); 
  } catch (error) {
    console.log(error);
    res.status(500).json({ errorMessage: "Failed to create unpaid order" })
  }
});

app.post("/create-paid-order", async (req, res) => {
  let { payload, user } = req.body;
  try {
    const userFromDB = await User.findOne({ 
      UserName: user.UserName 
    }).exec();
    const unpaidOrderFromDB = await UnpaidOrder.findOne({ 
      User: userFromDB 
    }).exec();
    await new PaidOrder({ 
      Items: unpaidOrderFromDB.Items, 
      TotalCost: unpaidOrderFromDB.TotalCost, 
      User: unpaidOrderFromDB.User, 
      Payload: payload 
    }).save();
    await UnpaidOrder.findByIdAndDelete(unpaidOrderFromDB._id).exec();
    res.json({ successMessage: "Order created" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ errorMessage: `Failed to create order, please contact helpdesk@your-company.com for assistance, use the reference ${payload.paymentIntent.client_secret}` })
  };
});

port = process.env.PORT || 8000;
app.listen(port, () => { console.log(`Listening on port ${port}`) });
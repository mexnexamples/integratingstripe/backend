const Item = require('./models/Item');
const UnpaidOrder = require('./models/UnpaidOrder');
const User = require('./models/User');
const calculateCost = async (items, user) => {
    let totalCost = 0;
    let itemsFromDB = [];
    for (const item of items) {
        try {
            const itemFromDB = await Item.findById(item._id).exec();
            itemsFromDB.push({ItemToBuy: itemFromDB, Amount: item.Amount});
            totalCost += item.Amount * itemFromDB.Price;
        } catch (error) {
            console.log(error);
        };
    };
    totalCost = totalCost * 100;        // Stripe uses cents, so we have to multiply by 100
    const userInDB = await User.findOne({UserName: user.UserName}).exec();
    UnpaidOrder.findOneAndDelete({User: userInDB}).exec();                // Used to ensure there's only one unpaid order at a time
    const unpaidOrder = await new UnpaidOrder({Items: itemsFromDB, TotalCost: totalCost, User: userInDB}).save();
    return unpaidOrder;
};

module.exports = calculateCost;